﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pomidorka.Client.Dto;

namespace Pomidorka.Client.Service
{
    public interface IPomidorkaService
    {
        PomidorkaDto StartPomidorka(StartRequestDto pomidorka);
        PomidorkaDto Status();
        PomidorkaDto Abort(string id);
    }
}
