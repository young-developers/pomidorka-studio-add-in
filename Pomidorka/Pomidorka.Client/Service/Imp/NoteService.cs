﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pomidorka.Client.Dto;
using Newtonsoft.Json;

namespace Pomidorka.Client.Service.Imp
{
    public class NoteService : INoteService
    {
        private const string BaseUrl = "http://91.225.131.53:8080/pomidorka-ws-1.0-SNAPSHOT/api/";
        public NoteDto CreateNote(string payload)
        {
            string responce = WebRequests.Post(BaseUrl + "note/create", "payload=" + payload, "application/x-www-form-urlencoded");

            return JsonConvert.DeserializeObject<NoteDto>(responce);
        }

        public List<NoteDto> Notes()
        {
            string responce = WebRequests.Get(BaseUrl + "note", "");

            return JsonConvert.DeserializeObject<List<NoteDto>>(responce);
        }

        public NoteDto Update(string id, string payload, bool isCompleted)
        {
            string responce = WebRequests.Post(BaseUrl + "note/update", "id=" + id + "&payload=" + payload + "&isCompleted=" + isCompleted, "application/x-www-form-urlencoded");

            return JsonConvert.DeserializeObject<NoteDto>(responce);
        }
    }
}
