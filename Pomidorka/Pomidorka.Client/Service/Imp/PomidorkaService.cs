﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pomidorka.Client.Dto;
using Newtonsoft.Json;

namespace Pomidorka.Client.Service.Imp
{
    public class PomidorkaService : IPomidorkaService
    {
        //private const string BaseUrl = "http://pomidorka-marnikitta.rhcloud.com/api/";

        private const string BaseUrl = "http://91.225.131.53:8080/pomidorka-ws-1.0-SNAPSHOT/api/";
        public PomidorkaDto Abort(string id)
        {
            string responce = WebRequests.Post(BaseUrl + "pomidorka/abort", id);

            return JsonConvert.DeserializeObject<PomidorkaDto>(responce);
        }

        public PomidorkaDto Status()
        {
            string responce = WebRequests.Get(BaseUrl + "pomidorka/status", "");

            return JsonConvert.DeserializeObject<PomidorkaDto>(responce);
        }

        public PomidorkaDto StartPomidorka(StartRequestDto pomidorka)
        {
            string output = JsonConvert.SerializeObject(pomidorka);

            string responce = WebRequests.Post(BaseUrl + "pomidorka/start", output);

            return JsonConvert.DeserializeObject<PomidorkaDto>(responce);
        }
    }
}
