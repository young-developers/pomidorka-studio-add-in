﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pomidorka.Client.Dto;

namespace Pomidorka.Client.Service
{
    public interface INoteService
    {
        NoteDto CreateNote(string payload);
        List<NoteDto> Notes();
        NoteDto Update(string id, string payload, bool isCompleted);
    }
}
