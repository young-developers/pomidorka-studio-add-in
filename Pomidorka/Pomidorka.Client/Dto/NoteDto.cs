﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Pomidorka.Client.Dto
{
    public class NoteDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("createTs")]
        public long CreateTs { get; set; }
        [JsonProperty("lastUpdateTs")]
        public long LastUpdateTs { get; set; }
        [JsonProperty("payload")]
        public string Payload { get; set; }
        [JsonProperty("isCompleted")]
        public bool IsCompleted { get; set; }
    }
}
