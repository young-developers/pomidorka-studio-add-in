﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Pomidorka.Client.Dto
{
    public class PomidorkaDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("serverTs")]
        public long ServerTs { get; set; }
        [JsonProperty("startTs")]
        public long StartTs { get; set; }
        [JsonProperty("stopTs")]
        public long StopTs { get; set; }
        [JsonProperty("type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public TypeDto Type { get; set; }
        [JsonProperty("status")]
        [JsonConverter(typeof(StringEnumConverter))]
        public StatusDto Status { get; set; }
    }
}
