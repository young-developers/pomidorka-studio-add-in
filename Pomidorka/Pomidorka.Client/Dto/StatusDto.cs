﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pomidorka.Client.Dto
{
    public enum StatusDto
    {
        RUNNING,
        ABORTED,
        FINISHED
    }
}
