﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Pomidorka.Application.Service.Imp;
using Pomidorka.Application.Service;
using Pomidorka.Application.Repository;
using Pomidorka.Domain.Model;

namespace Pomidorka.Application
{
    public class UpdateTask
    {
        public static void Update(object state)
        {
            for (;;)
            {
                IPomidorkaService service = new PomidorkaService();

                PomidorkaRepository.UpdatePomidorka(service.Status());

                Thread.Sleep(1000);
            }
        }

        public static void UpdateNotes(object state)
        {
            for (;;)
            {
                INoteService service = new NoteService();

                List<Note> newNotes = service.Notes();

                NoteRepository.UpdateNotes(newNotes);

                Thread.Sleep(10000);
            }
        }
    }
}
