﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pomidorka.Domain.Model;

namespace Pomidorka.Application.Service
{
    public interface IPomidorkaService
    {
        Domain.Model.Pomidorka StartPomidorka(Domain.Model.Pomidorka pomidorka);
        Domain.Model.Pomidorka Status();
        Domain.Model.Pomidorka Abort(Guid id);
    }
}
