﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pomidorka.Domain.Model;
using Pomidorka.Client.Dto;

namespace Pomidorka.Application.Service.Imp
{
    public class NoteService : INoteService
    {
        private long ToUnixTimeStamp(DateTime time)
        {
            return Convert.ToInt64(time.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds);
        }
        private DateTime FromUnixTimeStamp(long mills)
        {
            var posixTime = DateTime.SpecifyKind(new DateTime(1970, 1, 1), DateTimeKind.Utc);
            return posixTime.AddMilliseconds(mills).ToLocalTime();
        }
        private Client.Service.INoteService service = new Client.Service.Imp.NoteService();
        public Note CreateNote(string payload)
        {
            NoteDto noteResponce = service.CreateNote(payload);

            return GetNote(noteResponce);
        }

        public List<Note> Notes()
        {
            List<NoteDto> notesResponce = service.Notes();

            var listNotes = new List<Note>();

            foreach (var noteDto in notesResponce)
            {
                listNotes.Add(GetNote(noteDto));
            }

            return listNotes;
        }

        public Note Update(Guid id, string payload, bool isCompleted)
        {
            NoteDto noteResponce = service.Update(id.ToString(), payload, isCompleted);

            return GetNote(noteResponce);
        }
        private Note GetNote(NoteDto noteDto)
        {
            return new Note(new Guid(noteDto.Id), noteDto.IsCompleted, FromUnixTimeStamp(noteDto.CreateTs), FromUnixTimeStamp(noteDto.LastUpdateTs), noteDto.Payload);
        }
    }
}
