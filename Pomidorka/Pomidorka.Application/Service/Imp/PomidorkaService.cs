﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pomidorka.Domain.Model;
using Pomidorka.Client.Dto;

namespace Pomidorka.Application.Service.Imp
{
   public class PomidorkaService : IPomidorkaService
    {
        private long ToUnixTimeStamp(DateTime time)
        {
            return Convert.ToInt64(time.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds);
        }
        private DateTime FromUnixTimeStamp(long mills)
        {
            var posixTime = DateTime.SpecifyKind(new DateTime(1970, 1, 1), DateTimeKind.Utc);
            return posixTime.AddMilliseconds(mills).ToLocalTime();
        }
        private Client.Service.IPomidorkaService service = new Client.Service.Imp.PomidorkaService();
        public Domain.Model.Pomidorka StartPomidorka(Domain.Model.Pomidorka pomidorka)
        {
            StartRequestDto pomidorkaDto = new StartRequestDto()
            {
                StartTs = ToUnixTimeStamp(pomidorka.StartTs),
                ClientTs = ToUnixTimeStamp(new DateTime()),
                StopTs = ToUnixTimeStamp(pomidorka.StopTs),
                Type = (TypeDto) Enum.Parse(typeof(Domain.Model.Type), pomidorka.Type.ToString())
            };

            PomidorkaDto pomidorkaResponce = service.StartPomidorka(pomidorkaDto);

            return GetPomidorka(pomidorkaResponce);
        }
        public Domain.Model.Pomidorka Status()
        {
            PomidorkaDto pomidorkaResponce = service.Status();

            return GetPomidorka(pomidorkaResponce);
        }
        public Domain.Model.Pomidorka Abort(Guid id)
        {
            PomidorkaDto pomidorkaResponce = service.Abort(id.ToString());

            return GetPomidorka(pomidorkaResponce);
        }

        private Domain.Model.Pomidorka GetPomidorka(PomidorkaDto pomidorkaDto)
        {
            return new Domain.Model.Pomidorka(new Guid(pomidorkaDto.Id), FromUnixTimeStamp(pomidorkaDto.ServerTs),
                FromUnixTimeStamp(pomidorkaDto.StartTs), FromUnixTimeStamp(pomidorkaDto.StopTs),
                (Domain.Model.Type)Enum.Parse(typeof(TypeDto), pomidorkaDto.Type.ToString()),
                (Status)Enum.Parse(typeof(StatusDto), pomidorkaDto.Status.ToString()));
        }
    }
}
