﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pomidorka.Application.Service
{
    public interface INoteService
    {
        Domain.Model.Note CreateNote(string payload);
        List<Domain.Model.Note> Notes();
        Domain.Model.Note Update(Guid id, string payload, bool isCompleted);
    }
}
