﻿using Pomidorka.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pomidorka.Application.Repository
{
    public static class NoteRepository
    {
        private static List<Note> notes;
        public static void UpdateNotes(List<Note> newNotes)
        {
            notes = newNotes;
        }
        public static List<Note> GetNotes()
        {
            return notes;
        }
    }
}
