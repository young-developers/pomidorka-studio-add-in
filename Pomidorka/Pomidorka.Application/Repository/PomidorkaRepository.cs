﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pomidorka.Application.Repository
{
    public static class PomidorkaRepository
    {
        private static UpdateListener listener;
        private static Domain.Model.Pomidorka currentPomidorka;
        public delegate void UpdateListener(Domain.Model.Pomidorka oldPom, Domain.Model.Pomidorka newPom);
        public static void OnUpdate(UpdateListener del)
        {
            listener = del;
        }

        public static void UpdatePomidorka(Domain.Model.Pomidorka newPomidorka)
        {
            listener.Invoke(currentPomidorka, newPomidorka);
            currentPomidorka = newPomidorka;
        }
        public static Domain.Model.Pomidorka GetCurrentPomidorka()
        {
            return currentPomidorka;
        }
    }
}
