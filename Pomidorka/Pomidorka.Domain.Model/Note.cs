﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pomidorka.Domain.Model
{
    public class Note
    {
        public Note(Guid id, bool isCompleted, DateTime createTs, DateTime lastUpdateTs, string payload)
        {
            Id = id;
            IsCompleted = isCompleted;
            CreateTs = createTs;
            LastUpdateTs = lastUpdateTs;
            Payload = payload;
        }

        public Guid Id { get; }
        public bool IsCompleted { get; set; }
        public DateTime CreateTs { get; }
        public DateTime LastUpdateTs { get; }
        public string Payload { get; }
    }
}
