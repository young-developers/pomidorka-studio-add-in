﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pomidorka.Domain.Model
{
    public class Pomidorka
    {
        public Pomidorka(Guid id, DateTime serverTs, DateTime startTs, DateTime stopTs, Type type, Status status)
        {
            Id = id;
            ServerTs = serverTs;
            StartTs = startTs;
            StopTs = stopTs;
            Type = type;
            Status = status;
        }

        public Guid Id { get; }
        public DateTime ServerTs { get; }
        public DateTime StartTs { get; }
        public DateTime StopTs { get; }
        public Type Type { get; }
        public Status Status { get; }
    }
}
