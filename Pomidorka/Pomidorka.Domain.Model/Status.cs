﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pomidorka.Domain.Model
{
    public enum Status
    {
        RUNNING,
        ABORTED,
        FINISHED
    }
}
