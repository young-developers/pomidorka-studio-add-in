﻿//------------------------------------------------------------------------------
// <copyright file="PomidorkaMenuCommand.cs" company="Company">
//     Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel.Design;
using System.Globalization;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Pomidorka.Application.Service;
using Pomidorka.Application.Service.Imp;
using Pomidorka.Application.Repository;
using System.Threading;
using Pomidorka.Application;
using System.Drawing;

namespace Pomidorka.Front
{
    /// <summary>
    /// Command handler
    /// </summary>
    internal sealed class PomidorkaMenuCommand
    {
        IVsStatusbar statusBar;
        /// <summary>
        /// Command ID.
        /// </summary>
        public const int CommandId = 0x0100;

        public const int StartPomidorkaCommandId = 0x1060;

        public const int StartRestPomidorkaCommandId = 0x1070;

        public const int AbortPomidorkaCommandId = 0x1080;

        /// <summary>
        /// Command menu group (command set GUID).
        /// </summary>
        public static readonly Guid CommandSet = new Guid("8765707b-571e-42b0-97fd-de244dc7980e");

        /// <summary>
        /// VS Package that provides this command, not null.
        /// </summary>
        private readonly Package package;

        /// <summary>
        /// Initializes a new instance of the <see cref="PomidorkaMenuCommand"/> class.
        /// Adds our command handlers for menu (commands must exist in the command table file)
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        private PomidorkaMenuCommand(Package package)
        {
            if (package == null)
            {
                throw new ArgumentNullException("package");
            }

            this.package = package;

            OleMenuCommandService commandService = this.ServiceProvider.GetService(typeof(IMenuCommandService)) as OleMenuCommandService;
            if (commandService != null)
            {
                var menuCommandID = new CommandID(CommandSet, CommandId);
                var menuItem = new MenuCommand(this.MenuItemCallback, menuCommandID);
                commandService.AddCommand(menuItem);
            }

            var pomidorkaStartCommandId = new CommandID(CommandSet, StartPomidorkaCommandId);
            var pomidorkaStartItem = new MenuCommand(this.StartPomidorkaItemCallback, pomidorkaStartCommandId);
            commandService.AddCommand(pomidorkaStartItem);

            var pomidorkaRestStartCommandId = new CommandID(CommandSet, StartRestPomidorkaCommandId);
            var pomidorkaRestStartItem = new MenuCommand(this.StartRestPomidorkaItemCallback, pomidorkaRestStartCommandId);
            commandService.AddCommand(pomidorkaRestStartItem);

            var pomidorkaAbortCommandId = new CommandID(CommandSet, AbortPomidorkaCommandId);
            var pomidorkaAbortItem = new MenuCommand(this.AbortPomidorkaItemCallback, pomidorkaAbortCommandId);
            commandService.AddCommand(pomidorkaAbortItem);

            
            statusBar = (IVsStatusbar)ServiceProvider.GetService(typeof(SVsStatusbar));
            ThreadPool.QueueUserWorkItem(ThreadTimePomidorka, this);
            ThreadPool.QueueUserWorkItem(UpdateTask.Update);
            PomidorkaRepository.OnUpdate((oldPom, newPom) => 
            {
                if (oldPom != null && oldPom.Status == Domain.Model.Status.RUNNING && 
                (newPom.Status == Domain.Model.Status.FINISHED || newPom.Status == Domain.Model.Status.ABORTED))
                {
                    System.Threading.Tasks.Task.Run(() => VsShellUtilities.ShowMessageBox(
                        this.ServiceProvider,
                        "Pomidorka",
                        oldPom.Status.ToString() + " --> " + newPom.Status.ToString(),
                        OLEMSGICON.OLEMSGICON_INFO,
                        OLEMSGBUTTON.OLEMSGBUTTON_OK,
                        OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST));
                }
            });
        }

        /// <summary>
        /// Gets the instance of the command.
        /// </summary>
        public static PomidorkaMenuCommand Instance
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the service provider from the owner package.
        /// </summary>
        private IServiceProvider ServiceProvider
        {
            get
            {
                return this.package;
            }
        }

        /// <summary>
        /// Initializes the singleton instance of the command.
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        public static void Initialize(Package package)
        {
            Instance = new PomidorkaMenuCommand(package);
        }

        /// <summary>
        /// This function is the callback used to execute the command when the menu item is clicked.
        /// See the constructor to see how the menu item is associated with this function using
        /// OleMenuCommandService service and MenuCommand class.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event args.</param>
        private void MenuItemCallback(object sender, EventArgs e)
        {
            string message = string.Format(CultureInfo.CurrentCulture, "Inside {0}.MenuItemCallback()", this.GetType().FullName);
            string title = "PomidorkaMenuCommand";

            // Show a message box to prove we were here
            VsShellUtilities.ShowMessageBox(
                this.ServiceProvider,
                message,
                title,
                OLEMSGICON.OLEMSGICON_INFO,
                OLEMSGBUTTON.OLEMSGBUTTON_OK,
                OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST);
        }

        private void StartPomidorkaItemCallback(object sender, EventArgs e)
        {
            var pomidorka = new Domain.Model.Pomidorka(Guid.Empty, DateTime.Now, DateTime.Now, DateTime.Now.AddMinutes(25), Domain.Model.Type.WORK, Domain.Model.Status.RUNNING);

            IPomidorkaService service = new PomidorkaService();
            var pomRes = service.StartPomidorka(pomidorka);
        }

        private void StartRestPomidorkaItemCallback(object sender, EventArgs e)
        {
            var pomidorka = new Domain.Model.Pomidorka(Guid.Empty, DateTime.Now, DateTime.Now, DateTime.Now.AddMinutes(5), Domain.Model.Type.REST, Domain.Model.Status.RUNNING);

            IPomidorkaService service = new PomidorkaService();
            var pomRes = service.StartPomidorka(pomidorka);
        }

        private void AbortPomidorkaItemCallback(object sender, EventArgs e)
        {
            IPomidorkaService service = new PomidorkaService();
            var pomRes = service.Abort(PomidorkaRepository.GetCurrentPomidorka().Id);
        }

        private static void ThreadTimePomidorka(object stateInfo)
        {
            var ctrl = stateInfo as PomidorkaMenuCommand;
            for (;;)
            {
                Domain.Model.Pomidorka pomidorka = PomidorkaRepository.GetCurrentPomidorka();
                if (pomidorka == null || pomidorka.Status == Domain.Model.Status.ABORTED || pomidorka.Status == Domain.Model.Status.FINISHED)
                {
                    ctrl.statusBar.SetText("0 - Aborted");
                }
                else
                {
                    TimeSpan ts = PomidorkaRepository.GetCurrentPomidorka().StopTs - DateTime.Now;
                    if (PomidorkaRepository.GetCurrentPomidorka().Type == Domain.Model.Type.REST)
                        ctrl.statusBar.SetText(String.Format("{0}:{1}", ts.Minutes, ts.Seconds) + " - Relaxing");
                    else
                        ctrl.statusBar.SetText(String.Format("{0}:{1}", ts.Minutes, ts.Seconds) + " - Working");
                }
                Thread.Sleep(1000);
            }
        }


    }
}
