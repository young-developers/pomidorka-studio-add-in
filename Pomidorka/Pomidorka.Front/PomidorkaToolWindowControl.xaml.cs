﻿//------------------------------------------------------------------------------
// <copyright file="PomidorkaToolWindowControl.xaml.cs" company="Company">
//     Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Pomidorka.Front
{
    using System.Diagnostics.CodeAnalysis;
    using System.Windows;
    using System.Windows.Controls;
    using System.Threading;
    using Pomidorka.Application.Repository;
    using Pomidorka.Application;
    using System.Collections.Generic;
    using Pomidorka.Application.Service;
    using Pomidorka.Application.Service.Imp;
    using Domain.Model;

    public partial class PomidorkaToolWindowControl : UserControl
    {
        StackPanel panel;
        /// <summary>
        /// Initializes a new instance of the <see cref="PomidorkaToolWindowControl"/> class.
        /// </summary>
        public PomidorkaToolWindowControl()
        {
            this.InitializeComponent();
            panel = notesPanel;

            ThreadPool.QueueUserWorkItem(UpdateTask.UpdateNotes, null);
            ThreadPool.QueueUserWorkItem(ThreadTimeNotes, this);
        }

        static Dictionary<CheckBox, Note> map = new Dictionary<CheckBox, Note>();

        private static void ThreadTimeNotes(object stateInfo)
        {
            Thread.Sleep(2000);
            var ctrl = stateInfo as PomidorkaToolWindowControl;
            for (;;)
            {
                List<Note> notes = NoteRepository.GetNotes();
               
                notes.Sort(delegate (Note x, Note y)
                {
                    if (x.CreateTs > y.CreateTs)
                        return -1;
                    else
                        return 1;
                });
                map = new Dictionary<CheckBox, Note>();

                ctrl.notesPanel.Dispatcher.Invoke(() => { ctrl.notesPanel.Children.Clear(); });
                foreach (Note note in notes)
                {
                    ctrl.notesPanel.Dispatcher.Invoke(() => 
                    {
                        StackPanel miniPanel = new StackPanel();
                        miniPanel.Orientation = Orientation.Horizontal;
                        CheckBox checker = new CheckBox();
                        map.Add(checker, note);
                        checker.VerticalAlignment = VerticalAlignment.Center;
                        checker.HorizontalAlignment = HorizontalAlignment.Center;
                        checker.Margin = new Thickness(10, 2, 2, 2);
                        if (note.IsCompleted)
                        {
                            checker.IsChecked = true;
                            checker.IsEnabled = false;
                        }
                        else
                        {
                            checker.IsChecked = false;
                            checker.IsEnabled = true;
                            checker.Click += Check;
                        }
                        Label label = new Label();
                        label.Content = note.Payload;
                        label.FontSize = 20;
                        miniPanel.Children.Add(checker);
                        miniPanel.Children.Add(label);
                        ctrl.notesPanel.Children.Add(miniPanel);
                    });
                }
                Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// Handles click on the button by displaying a message box.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event args.</param>
        [SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions", Justification = "Sample code")]
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Default event handler naming pattern")]
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(
                string.Format(System.Globalization.CultureInfo.CurrentUICulture, "Invoked '{0}'", this.ToString()),
                "PomidorkaToolWindow");
        }

        private void addNoteButton_Click(object sender, RoutedEventArgs e)
        {
            INoteService service = new NoteService();
            Note newNote = service.CreateNote(noteText.Text);
            noteText.Text = "";
            List<Note> notes = NoteRepository.GetNotes();
            notes.Add(newNote);
            NoteRepository.UpdateNotes(notes);
        }
        private static void Check(object sender, RoutedEventArgs e)
        {
            Note note;
            map.TryGetValue(sender as CheckBox, out note);
            INoteService service = new NoteService();
            Note newNote = service.Update(note.Id, note.Payload, true);
            List<Note> notes = NoteRepository.GetNotes();
            notes.Find(n => n.Id == note.Id).IsCompleted = true;
            NoteRepository.UpdateNotes(notes);
        }
    }
}